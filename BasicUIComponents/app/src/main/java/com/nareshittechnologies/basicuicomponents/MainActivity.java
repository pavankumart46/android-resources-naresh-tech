package com.nareshittechnologies.basicuicomponents;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ToggleButton;

import org.w3c.dom.Text;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity implements AdapterView.OnItemSelectedListener, CompoundButton.OnCheckedChangeListener {

    EditText personName;
    Spinner spinner, spinner2;
    RadioGroup rg;
    CheckBox tel,eng,hin;
    ToggleButton toggleButton;
    Switch aSwitch;
    TextView result;

    String country = "No Item Selected";
    String[] indian_states = new String[]{"Select a State","Andhra","Tamil nadu","UP","HP","J&K","Gujrat","Telangana"};
    String[] japan_states = new String[]{"Select a State","Hokkaidō Hokkaidō","Tokyo","kasukabaia"};
    String gender = "prefer not to say";
    List<String> languages = new ArrayList<>();
    boolean toggle = false , s = false;
    String state = "Nothing selected";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initViews();

        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this,
                R.array.countries, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(adapter);

        spinner.setOnItemSelectedListener(this);
        spinner2.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                 state = adapterView.getItemAtPosition(i).toString();
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
        /*spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                if(i != 0) {
                    String val = adapterView.getItemAtPosition(i).toString();
                    Toast.makeText(MainActivity.this, val, Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });*/

        rg.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int i) {
                if(radioGroup.getCheckedRadioButtonId() == R.id.male){
                    gender = "Male";
                }else if(radioGroup.getCheckedRadioButtonId() == R.id.female){
                    gender = "Female";
                }else{
                    gender = "Prefer not to Say";
                }
                Toast.makeText(MainActivity.this, gender, Toast.LENGTH_SHORT).show();
            }
        });
        
        tel.setOnCheckedChangeListener(this);

        hin.setOnCheckedChangeListener(this);

        eng.setOnCheckedChangeListener(this);
        
        toggleButton.setOnCheckedChangeListener(this);

        aSwitch.setOnCheckedChangeListener(this);
    }

    /**This method will be invoked when the button is pressed on the activity_main.xml*/
    public void getDetails(View view) {
        String n = personName.getText().toString();
        if(n.isEmpty()){
            personName.setError("NAME IS MANDATORY");
            TextView ss = (TextView) spinner.getSelectedView();
            ss.setError("ERROR");
        }else{
            result.setText(n+"\n");
            result.append(country+"\n");
            result.append(state+"\n");
            result.append(gender+"\n");
            for(String lan : languages){
                result.append(lan+"\t");
            }
            result.append("\n");
        }
    }

    private void initViews() {
        personName = findViewById(R.id.name_et);
        spinner = findViewById(R.id.country_spin);
        rg = findViewById(R.id.radio_group);
        tel = findViewById(R.id.lang_tel);
        eng = findViewById(R.id.lang_eng);
        hin = findViewById(R.id.lang_hin);
        toggleButton = findViewById(R.id.toggle_button);
        aSwitch = findViewById(R.id.switch_id);
        result = findViewById(R.id.result);
        spinner2 = findViewById(R.id.state_spin);
    }

    @Override
    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
            if(i != 0) {
                String[] items = indian_states;
                country= adapterView.getItemAtPosition(i).toString();
                Toast.makeText(this, country, Toast.LENGTH_SHORT).show();
                if(i==1){
                    items = indian_states;
                }else if(i==3){
                    items = japan_states;
                }
                ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,
                        android.R.layout.simple_spinner_item,items);
                adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                spinner2.setAdapter(adapter);
            }

    }

    @Override
    public void onNothingSelected(AdapterView<?> adapterView) {

    }

    @Override
    public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
        switch(compoundButton.getId()){
            case R.id.lang_tel:
                // logic
                if(b){
                    if(!languages.contains("Telugu"))
                        languages.add("Telugu");
                }else{
                    if(languages.contains("Telugu"))
                        languages.remove("Telugu");
                }
                break;
            case R.id.lang_hin:
                // logic
                if(b){
                    if(!languages.contains("Hindi"))
                        languages.add("Hindi");
                }else{
                    if(languages.contains("Hindi"))
                        languages.remove("Hindi");
                }
                break;
            case R.id.lang_eng:
                // logic
                if(b){
                    if(!languages.contains("English"))
                        languages.add("English");
                }else{
                    if(languages.contains("English"))
                        languages.remove("English");
                }
                break;

            case R.id.toggle_button:
                if(b){
                    toggle = true;
                }else{
                    toggle = false;
                }
                break;

            case R.id.switch_id:
                if(b){
                   s = true;
                }else{
                    s = false;
                }
                break;
        }
    }
}