package com.nareshittechnologies.notificationsexample;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.NotificationCompat;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.view.View;

public class MainActivity extends AppCompatActivity {

    public static final int NOTIFICATION_ID = 23;
    NotificationManager manager;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        manager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
    }

    public void sendNotification(View view) {
        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.O){
            NotificationChannel channel = new NotificationChannel("noti_chann","Pavan Channel",
                    NotificationManager.IMPORTANCE_HIGH);
            channel.setDescription("Any description that you would like to give");
            channel.setLightColor(Color.RED);

            // Create notification channel with the help of Notification Manager
            manager.createNotificationChannel(channel);
        }

        NotificationCompat.Builder builder = new NotificationCompat.Builder(this,"noti_chann");
        builder.setSmallIcon(R.drawable.umbrella);
        builder.setAutoCancel(true);
        builder.setContentTitle("Hello, How are you ?");
        builder.setContentText("This is the description");
        builder.setPriority(NotificationCompat.PRIORITY_HIGH);
        builder.setStyle(new NotificationCompat.BigTextStyle().bigText(getResources().getString(R.string.big_text)));

        // The action is - Want to open MainActivity when the notification is tapped.
        Intent i = new Intent(this,MainActivity.class);

        PendingIntent pi = PendingIntent.getActivity(this,23,i,PendingIntent.FLAG_UPDATE_CURRENT);

        builder.setContentIntent(pi);

        builder.addAction(R.drawable.umbrella,"ACTION",pi);
        builder.addAction(R.drawable.umbrella,"ACTION",pi);
        builder.addAction(R.drawable.umbrella,"ACTION",pi);

        manager.notify(NOTIFICATION_ID,builder.build());
    }

    public void cancelNotification(View view) {
        manager.cancel(NOTIFICATION_ID);
    }

    /**
     * Three Important components
     * - NotificationManager - Manages your notifications - Create, Cancel ...
     * - Notification - That builds your notification - setIcon, setTitle, SetMessages...
     * - NotificationChannel - Will create a Notification Channel*/
}