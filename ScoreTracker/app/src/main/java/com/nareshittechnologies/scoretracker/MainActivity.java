package com.nareshittechnologies.scoretracker;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {
    public static final String KEY_COUNT = "COUNT";
    TextView textView;
    int count = 0;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        // Connecting the textView object with the TextView on xml using the id (findViewById(...))
        textView = findViewById(R.id.result);
        // TODO 2: As soon as the activity comes to foreground, show the saved values
        if(savedInstanceState!=null && savedInstanceState.containsKey(KEY_COUNT)){
            count = savedInstanceState.getInt(KEY_COUNT);
            textView.setText(String.valueOf(count));
        }
    }

    /**
     * This method invoked as soon as the + button is clicked on the activity_main.xml
     * In this method, we will increment the current value on the TextView */
    public void incrementScore(View view) {
        count++;
        // setText(..) can be used to override the existing value on the textview with the new value
        // setText(...) can only display String values - you will encounter an error if you try display int
        // or floating point values etc
        textView.setText(String.valueOf(count));
    }

    /**
     * This method invoked as soon as the - button is clicked on the activity_main.xml
     * In this method, we will decrement the current value on the TextView */
    public void decrementScore(View view) {
        count--;
        textView.setText(String.valueOf(count));
    }

    // When there is a configuration change of the activity (potrait to landscape and viceversa)
    // The activity will restart - The UI will be reset
    //TODO 1: When the activity is about to shutdown, save the values - onSaveInstanceState(...)

    @Override
    protected void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);

        outState.putInt(KEY_COUNT,count);
    }


}