package com.nareshittechnologies.firebaserealtimedatabases;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.os.RecoverySystem;
import android.view.View;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    EditText name_et, age_et;
    DatabaseReference reference;
    ProgressBar progressBar;
    RecyclerView recyclerView;
    List<String> keys;
    List<Person> persons;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        name_et = findViewById(R.id.name_et);
        age_et = findViewById(R.id.age_et);
        progressBar = findViewById(R.id.progressBar);
        progressBar.setVisibility(View.INVISIBLE);
        recyclerView = findViewById(R.id.recyclerview);
        keys = new ArrayList<>();
        persons = new ArrayList<>();
        reference = FirebaseDatabase.getInstance().getReference();
    }

    public void saveData(View view) {
        String name = name_et.getText().toString();
        int age = Integer.parseInt(age_et.getText().toString());
        Person p = new Person(name,age);
        reference.child("Persons").push().setValue(p).addOnSuccessListener(new OnSuccessListener<Void>() {
            @Override
            public void onSuccess(Void unused) {
                Toast.makeText(MainActivity.this, "Successfully Inserted the Data!", Toast.LENGTH_SHORT).show();
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                Toast.makeText(MainActivity.this, "Failed to insert data", Toast.LENGTH_SHORT).show();
            }
        });
    }


    public void loadData(View view) {
        progressBar.setVisibility(View.VISIBLE);
        reference.child("Persons").orderByKey().addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                progressBar.setVisibility(View.INVISIBLE);
                for (DataSnapshot data:snapshot.getChildren()){
                    // We are successfully iterating through all the child nodes in the Persons database
                    // Toast.makeText(MainActivity.this, data.getKey(), Toast.LENGTH_SHORT).show();
                    keys.add(data.getKey());
                    Person p = data.getValue(Person.class);
                    persons.add(p);
                    Toast.makeText(MainActivity.this, p.getName(), Toast.LENGTH_SHORT).show();
                }
                DataAdapter dataAdapter = new DataAdapter(MainActivity.this,keys,persons);
                recyclerView.setAdapter(dataAdapter);
                recyclerView.setLayoutManager(new LinearLayoutManager(MainActivity.this));
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        });
    }
}