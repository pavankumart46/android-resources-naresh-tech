package com.nareshittechnologies.firebaserealtimedatabases;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

public class DetailsActivity extends AppCompatActivity {

    EditText key,name,age;
    DatabaseReference reference;
    String k;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_details);
        key = findViewById(R.id.key);
        name = findViewById(R.id.p_name);
        age = findViewById(R.id.p_age);

        reference  = FirebaseDatabase.getInstance().getReference();

        Intent i = getIntent();
        k = i.getStringExtra("KEY");
        key.setText(k);
        name.setText(i.getStringExtra("NAME"));
        age.setText(String.valueOf(i.getIntExtra("AGE",0)));
    }

    public void updateData(View view) {
        String n = name.getText().toString();
        int a = Integer.parseInt(age.getText().toString());
        Person p = new Person(n,a);
        reference.child("Persons").child(k).setValue(p).addOnSuccessListener(new OnSuccessListener<Void>() {
            @Override
            public void onSuccess(Void unused) {
                Toast.makeText(DetailsActivity.this, "UPDATE SUCCESSFUL!", Toast.LENGTH_SHORT).show();
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                Toast.makeText(DetailsActivity.this, "FAILED TO UPDATE!", Toast.LENGTH_SHORT).show();
            }
        });
    }


    public void deleteData(View view) {
        reference.child("Persons").child(k).removeValue().addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                Toast.makeText(DetailsActivity.this, "Failed to Delete!", Toast.LENGTH_SHORT).show();
            }
        }).addOnSuccessListener(new OnSuccessListener<Void>() {
            @Override
            public void onSuccess(Void unused) {
                Toast.makeText(DetailsActivity.this, "Successfully Deleted!", Toast.LENGTH_SHORT).show();
            }
        });
    }
}
