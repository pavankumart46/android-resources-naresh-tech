package com.nareshittechnologies.firebaserealtimedatabases;

import android.content.Context;
import android.content.Intent;
import android.text.Layout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

public class DataAdapter extends RecyclerView.Adapter<DataAdapter.DataViewHolder> {
    Context context;
    List<String> keys;
    List<Person> persons;

    public DataAdapter(Context context, List<String> keys, List<Person> persons) {
        this.context = context;
        this.keys = keys;
        this.persons = persons;
    }

    @NonNull
    @Override
    public DataViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new DataViewHolder(LayoutInflater.from(context).inflate(R.layout.one_item_design,parent,false));
    }

    @Override
    public void onBindViewHolder(@NonNull DataViewHolder holder, int position) {
        holder.name.setText(persons.get(position).getName());
        holder.age.setText(String.valueOf(persons.get(position).getAge()));
    }

    @Override
    public int getItemCount() {
        return persons.size();
    }

    public class DataViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        TextView name, age;
        public DataViewHolder(@NonNull View itemView) {
            super(itemView);
            name = itemView.findViewById(R.id.textView);
            age = itemView.findViewById(R.id.textView2);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            int position = getAdapterPosition();
            String key = keys.get(position);
            String p_n = persons.get(position).getName();
            int p_a = persons.get(position).getAge();


            Intent i = new Intent(context,DetailsActivity.class);
            i.putExtra("KEY",key);
            i.putExtra("NAME",p_n);
            i.putExtra("AGE",p_a);
            context.startActivity(i);
        }
    }
}
