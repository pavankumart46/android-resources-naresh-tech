package com.nareshittechnologies.firebaserealtimedatabases;

// Model class helps you in organising the data in the key and value pairs.
public class Person {
    String name;
    int age;

    public Person() {

    }

    public Person(String name, int age) {
        this.name = name;
        this.age = age;
    }

    public String getName() {
        return name;
    }

    public int getAge() {
        return age;
    }
}
