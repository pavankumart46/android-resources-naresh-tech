## SQLite commands
[try the commands](https://sqliteonline.com/)
## CREATE
```SQLite
Create TABLE students(stu_id integer primary key AUTOINCREMENT,stu_name text, stu_age integer);
```

## INSERT 

```SQLite
INSERT INTO students(stu_name,stu_age) VALUES("Krish",33),("Ajay",42);
```

## Fetch data - SELECT QUERY

```SQLite
SELECT * from students;
```

```SQLite
SELECT * from students where stu_age >30;
```

```SQLite
SELECT * from students where stu_age >30 LIMIT 1;
```

## UPDATE QUERY

```SQLite
UPDATE students SET stu_age = 31 where stu_id = 1;
```

```SQLite
update students set stu_name = "RAM", stu_age = 32 where stu_id = 1;
```

## DELETE QUERY

```SQlite
DELETE from students where stu_id = 3;
```

```SQlite
delete from students where stu_name = "Krish";
```

## DROP a Table (Completely remove the table)
```SQlite
drop TABLE students;
```