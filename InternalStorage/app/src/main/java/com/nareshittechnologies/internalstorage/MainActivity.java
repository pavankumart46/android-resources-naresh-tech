package com.nareshittechnologies.internalstorage;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.InputStreamReader;

public class MainActivity extends AppCompatActivity {

    EditText name, age;
    TextView result;
    private static final String FILE_NAME = "Naresh_Tech.txt";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        name = findViewById(R.id.et_name);
        age = findViewById(R.id.et_age);
        result = findViewById(R.id.textView);
    }

    public void saveData(View view) {
        String n = name.getText().toString();
        int a = Integer.parseInt(age.getText().toString());
        String data = "Name : "+n+"\n"+"Age: "+a+"\n";
        FileOutputStream fos;
        try {
            fos = openFileOutput(FILE_NAME,MODE_APPEND);
            fos.write(data.getBytes());
            fos.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void loadData(View view) {
        FileInputStream fis;
        try {
            fis = openFileInput(FILE_NAME);
            BufferedReader reader = new BufferedReader(new InputStreamReader(fis));
            String line;
            StringBuffer sb = new StringBuffer();
            while((line=reader.readLine())!=null){
                sb.append(line+"\n");
            }
            result.setText(sb.toString());
            Toast.makeText(this, sb.toString(), Toast.LENGTH_SHORT).show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * To open a file and write into it
     *  - FileOutputStream
     *          - to Initialize FileOutputStream object - openFileOutput()
     * To open a file and read contents
     *  - FileInputStream*/
}