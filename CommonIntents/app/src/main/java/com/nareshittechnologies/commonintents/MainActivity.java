package com.nareshittechnologies.commonintents;

import static android.provider.MediaStore.ACTION_IMAGE_CAPTURE;
import static android.provider.MediaStore.INTENT_ACTION_MEDIA_PLAY_FROM_SEARCH;

import androidx.activity.result.ActivityResult;
import androidx.activity.result.ActivityResultCallback;
import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContract;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityOptionsCompat;

import android.app.SearchManager;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.provider.AlarmClock;
import android.provider.MediaStore;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import java.net.URI;

public class MainActivity extends AppCompatActivity {

    EditText url_et, url_maps;
    ImageView cam_img;
    ActivityResultLauncher<Intent> launcher;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        url_et = findViewById(R.id.url_et);
        url_maps = findViewById(R.id.maps_et);
        cam_img = findViewById(R.id.img_cam);
        launcher = registerForActivityResult(new ActivityResultContracts.StartActivityForResult(),
                new ActivityResultCallback<ActivityResult>() {
                    @Override
                    public void onActivityResult(ActivityResult result) {
                       if(result.getResultCode() == RESULT_OK){
                           Intent data = result.getData();
                           Bitmap b = data.getParcelableExtra("data");
                           cam_img.setImageBitmap(b);
                       }
                    }
                });
    }

    public void openURL(View view) {
        String u = url_et.getText().toString();
        Uri uri = Uri.parse("https://"+u);
        Intent openBrowser = new Intent(Intent.ACTION_VIEW,uri);
        startActivity(openBrowser);
    }

    public void openMaps(View view) {
        String address = url_maps.getText().toString();
        // CREATE the urI scheme
        // 17.437520758996374, 78.44807470000003
        Uri u = Uri.parse("geo:0,0?q="+address);
        Intent i = new Intent(Intent.ACTION_VIEW,u);
        startActivity(i);
    }

    public void openMusic(View view) {
        Intent intent = new Intent(INTENT_ACTION_MEDIA_PLAY_FROM_SEARCH);
        intent.putExtra(MediaStore.EXTRA_MEDIA_FOCUS,
                MediaStore.Audio.Artists.ENTRY_CONTENT_TYPE);
        intent.putExtra(MediaStore.EXTRA_MEDIA_ARTIST, "SP Balu");
        intent.putExtra(SearchManager.QUERY, "SP Balu");
        startActivity(intent);
    }


    public void setAlarm(View view) {
        Intent intent = new Intent(AlarmClock.ACTION_SET_ALARM);
        intent.putExtra(AlarmClock.EXTRA_HOUR,7);
        intent.putExtra(AlarmClock.EXTRA_MINUTES,15);
        intent.putExtra(AlarmClock.EXTRA_IS_PM,false);
        intent.putExtra(AlarmClock.EXTRA_MESSAGE,"This is a new alarm");
        startActivity(intent);
    }

    public void openCamera(View view) {
        Intent intent = new Intent(ACTION_IMAGE_CAPTURE);
        launcher.launch(intent);
    }
}