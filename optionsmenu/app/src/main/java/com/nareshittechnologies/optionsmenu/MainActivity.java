package com.nareshittechnologies.optionsmenu;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    // The following method is overrided to attach the options_menu.xml to activity_main.xml
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.options_menu,menu);
        return super.onCreateOptionsMenu(menu);
    }

    // to get the options menu items to action, override the following method

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()){
            case R.id.first_item:
                Toast.makeText(this, "FIRST ITEM", Toast.LENGTH_SHORT).show();
                break;
            case R.id.second_item:
                Toast.makeText(this, "Second ITEM", Toast.LENGTH_SHORT).show();
                break;
            case 123456:
                Toast.makeText(this, "Third Item", Toast.LENGTH_SHORT).show();
        }
        return super.onOptionsItemSelected(item);
    }


    // using onClick, we can get the menu items work
    /*public void firstMenu(MenuItem item) {
        Toast.makeText(this, "Hello", Toast.LENGTH_SHORT).show();
    }*/

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        invalidateOptionsMenu();
        menu.add(0,123456,0,"NEW ITEM");
        menu.add("New Item 2");
        menu.removeItem(R.id.gitem1);
        return super.onPrepareOptionsMenu(menu);
    }
}