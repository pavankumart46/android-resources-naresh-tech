package com.nareshittechnologies.popupmenu;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.ContextWrapper;
import android.os.Build;
import android.os.Bundle;
import android.view.ContextThemeWrapper;
import android.view.Gravity;
import android.view.MenuItem;
import android.view.View;
import android.widget.PopupMenu;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    // This method invokes as soon as there is a click on the button
    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP_MR1)
    public void popupMenu(View view) {
        // TODO 1: Instantiate popupmenu object
        Context wrapper = new ContextThemeWrapper(this,R.style.popupstyle);
        PopupMenu menu = new PopupMenu(wrapper, view, Gravity.RIGHT);

        // TODO 2: attach the menu.xml to show it to the user
        menu.getMenuInflater().inflate(R.menu.popup_menu,menu.getMenu());

        menu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem menuItem) {
                Toast.makeText(MainActivity.this, menuItem.getTitle(), Toast.LENGTH_SHORT).show();
                return true;
            }
        });

        menu.show();
    }
}