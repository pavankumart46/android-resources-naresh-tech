package com.nareshittechnologies.jobscheduler;

import android.app.job.JobParameters;
import android.app.job.JobService;
import android.widget.Toast;

// TODO 1: Extend the class to JobService class and override the methods (onStartJob, onStopJob)
public class TaskService extends JobService {

    /**
     * - When the task is executed successfully we return false
     * - if the task is offloaded to a worker thread - return true
     *          - once the job is finished on the worker thread -
     *          you should call - jobFinished() from it.*/
    @Override
    public boolean onStartJob(JobParameters jobParameters) {
        Toast.makeText(this, "Job Started!", Toast.LENGTH_SHORT).show();
        return false;
    }

    /**
     * true - when the task fails, you want to reschedule
     * false - when the task fails, you don't want to reschedule */
    @Override
    public boolean onStopJob(JobParameters jobParameters) {
        return false;
    }
}
