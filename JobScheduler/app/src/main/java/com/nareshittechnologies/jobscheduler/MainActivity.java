package com.nareshittechnologies.jobscheduler;

import androidx.appcompat.app.AppCompatActivity;

import android.app.job.JobInfo;
import android.app.job.JobScheduler;
import android.content.ComponentName;
import android.os.Bundle;
import android.view.View;

public class MainActivity extends AppCompatActivity {
    /**
     * JobScheduler was introduced with API level 21 (lollipop) - It does not work on the older devices
     * To Have the job Scheduler running on older versions you may refer to FirebaseJobDispatcher
     * https://developer.android.com/topic/libraries/architecture/workmanager/migrating-fb */

    JobInfo jobInfo;
    JobScheduler scheduler;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        scheduler = (JobScheduler) getSystemService(JOB_SCHEDULER_SERVICE);

        jobInfo = new JobInfo.Builder(123,new ComponentName(getPackageName(),TaskService.class.getName()))
                .setRequiresCharging(true)
                .build();

    }

    public void scheduleJob(View view) {
        // To Schedule the job
        scheduler.schedule(jobInfo);
    }

    /**
     * Steps involved
     * - JobService
     *      - onStartJob(...) - Define the Task and return false if the task is performed
     *      - onStopJob(...) - Return false as we don't want to reschedule the task
     *                       - Task can be rescheduled only when the task fails in between
     * - Register the Service in AndroidManifest.xml
     *
     * - JobInfo
     *      - We can set the required conditions defining when you want your task to be executed
     *
     * - JobScheduler
     *      - We can schedule or cancel the tasks
     *                       */
}