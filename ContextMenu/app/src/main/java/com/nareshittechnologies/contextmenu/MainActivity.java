package com.nareshittechnologies.contextmenu;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.ContextMenu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;

public class MainActivity extends AppCompatActivity {

    Button btn;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        btn = findViewById(R.id.button);
        registerForContextMenu(btn);
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo);
        getMenuInflater().inflate(R.menu.context_menu,menu);
        menu.setHeaderTitle("Choose an Option");
    }

    // super(....) - call to super class/ parent class constructor
    // super.methodName(...) - call the super class method

    @Override
    public boolean onContextItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()){
            case R.id.first:
                // logic
                btn.setText("FIRST OPTION");
                break;
            case R.id.second:
                // logic
                btn.setText("SECOND OPTION");
                break;
        }
        return super.onContextItemSelected(item);
    }
}

/*
* Context Menu appears when the user press long click on any item/element
* Context Menu is also called as Floating menu
* It doesnt support Icons*/