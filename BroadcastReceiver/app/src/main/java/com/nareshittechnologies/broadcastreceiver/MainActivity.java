package com.nareshittechnologies.broadcastreceiver;

import androidx.appcompat.app.AppCompatActivity;

import android.content.IntentFilter;
import android.os.Bundle;

public class MainActivity extends AppCompatActivity {

    public static final String ACTION_CUSTOM_BROADCAST =
            "com.nareshittechnologies.broadcastsender.CUSTOM_BROADCAST";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        IntentFilter i = new IntentFilter(ACTION_CUSTOM_BROADCAST);
        registerReceiver(new CustomBroadcastReceiver(),i);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        unregisterReceiver(new CustomBroadcastReceiver());
    }
}