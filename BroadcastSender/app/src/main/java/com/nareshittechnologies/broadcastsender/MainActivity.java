package com.nareshittechnologies.broadcastsender;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.view.View;

public class MainActivity extends AppCompatActivity {

    public static final String ACTION_CUSTOM_BROADCAST =
            "com.nareshittechnologies.broadcastsender.CUSTOM_BROADCAST";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        IntentFilter i = new IntentFilter(ACTION_CUSTOM_BROADCAST);
        registerReceiver(new CustomReceiver(),i);
    }

    public void sendCustomBroadcast(View view) {
        Intent customBroadcastIntent = new Intent(ACTION_CUSTOM_BROADCAST);
        sendBroadcast(customBroadcastIntent);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        unregisterReceiver(new CustomReceiver());
    }
}