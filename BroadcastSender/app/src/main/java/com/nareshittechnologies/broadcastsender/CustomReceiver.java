package com.nareshittechnologies.broadcastsender;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.widget.Toast;

public class CustomReceiver extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {
        if(intent.getAction() == MainActivity.ACTION_CUSTOM_BROADCAST){
            Toast.makeText(context, "Received in the same app", Toast.LENGTH_SHORT).show();
        }
    }
}