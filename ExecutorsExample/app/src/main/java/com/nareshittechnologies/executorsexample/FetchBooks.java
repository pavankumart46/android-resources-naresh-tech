package com.nareshittechnologies.executorsexample;

import android.content.Context;
import android.os.AsyncTask;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;

import javax.net.ssl.HttpsURLConnection;

public class FetchBooks extends AsyncTask<String,Void,String> {

    Context context;
    TextView result;
    ProgressBar progressBar;

    public FetchBooks(Context context, TextView result, ProgressBar progressBar) {
        this.context = context;
        this.result = result;
        this.progressBar = progressBar;
    }

    // doInBackground() works in the worker thread in the background
    // You cannot access the UI tool kit (such as TextViews, progressbars... ) from with in the doInBackground()
    @Override
    protected String doInBackground(String... strings) {
        String link = strings[0];
        try {
            URL url = new URL(link);
            HttpsURLConnection connection = (HttpsURLConnection) url.openConnection();
            InputStream is = connection.getInputStream();
            BufferedReader br = new BufferedReader(new InputStreamReader(is));
            String line;
            StringBuilder sb = new StringBuilder();
            while((line=br.readLine())!=null){
                sb.append(line);
            }
            return sb.toString();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    // onPostExecute() works in the Main or UI thread
    @Override
    protected void onPostExecute(String s) {
        super.onPostExecute(s);
        progressBar.setVisibility(View.GONE);
        result.setText(s);
    }
}
