package com.nareshittechnologies.executorsexample;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.concurrent.Executor;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import javax.net.ssl.HttpsURLConnection;

public class MainActivity extends AppCompatActivity {

    TextView result;
    ProgressBar progressBar;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        result = findViewById(R.id.textView);
        progressBar = findViewById(R.id.progressBar);
        progressBar.setVisibility(View.GONE);
    }

    public void loadData(View view) {
        progressBar.setVisibility(View.VISIBLE);
        String link = "https://www.googleapis.com/books/v1/volumes?q=quilting/";
        /*FetchBooks fetchBooks = new FetchBooks(this, result, progressBar);
        fetchBooks.execute(link);*/

        final StringBuilder[] sb = {new StringBuilder()};

        // Step 1: create an ExecutorService object
        ExecutorService service = Executors.newSingleThreadExecutor();
        // Step 2: Create the Executor
        service.execute(new Runnable() {
            // This run method is the replacement of doInBackground()
            // This works on Worker thread (Separate thread) in the background
            @Override
            public void run() {
                /*//if you want to preset the task that you want to run()
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        // Code here for onPreExecute()
                    }
                });*/
                // Place the code that you usually write in doInBackground()
                try {
                    URL url = new URL(link);
                    HttpsURLConnection connection = (HttpsURLConnection) url.openConnection();
                    InputStream is = connection.getInputStream();
                    BufferedReader br = new BufferedReader(new InputStreamReader(is));
                    String line;
                    sb[0] = new StringBuilder();
                    while((line=br.readLine())!=null){
                        sb[0].append(line);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }

                // runOnUiThread() is used as the replacement of onPostExecute()
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        progressBar.setVisibility(View.GONE);
                        result.setText(sb[0].toString());
                    }
                });
            }
        });
    }
}