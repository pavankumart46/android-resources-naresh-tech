package com.nareshittechnologies.newsinshort;

import android.content.Context;
import android.os.AsyncTask;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;

import javax.net.ssl.HttpsURLConnection;

public class FetchNews extends AsyncTask<String,Void,String> {

    Context context;
    RecyclerView recyclerView;
    ProgressBar progressBar;

    public FetchNews(Context context, RecyclerView recyclerView,ProgressBar progressBar) {
        this.context = context;
        this.recyclerView = recyclerView;
        this.progressBar = progressBar;
    }

    /**The Responsibility of doInBackground(...) method is to perform a long running task
     * In this case - Fetch the data from a URL (API).
     * Once doInBackground(...) finishes off its task, the results will be returned to
     * onPostExecute(...) method*/
    @Override
    protected String doInBackground(String... strings) {
        String link = strings[0]; // Assuming that the url will be sent to doInBackground(...)
        try {
            //TODO 1: First convert the string link into URL type
            URL url = new URL(link);
            //TODO 2: Establish a HttpsURLConnection with the link
            HttpsURLConnection connection = (HttpsURLConnection) url.openConnection();
            // TODO 3: Get the inputStream from the Established HttpsURLConnection
            InputStream input = connection.getInputStream();
            // TODO 4: Read the InputStream (Data) and return the results.
            BufferedReader br = new BufferedReader(new InputStreamReader(input));
            String line = "";
            StringBuffer buffer = new StringBuffer();
            while ((line=br.readLine())!=null){
                buffer.append(line);
            }
            return buffer.toString();
        }catch (Exception e){
            e.printStackTrace();
        }
        return null;
    }

    @Override
    protected void onPostExecute(String s) {
        super.onPostExecute(s);
       /* try{
            progressBar.setVisibility(View.GONE);
            textView.setText("");
            JSONObject root = new JSONObject(s);
            boolean isDataFetched = root.getBoolean("success");
            if(isDataFetched){
                // proceed to parse the response
                JSONArray items = root.getJSONArray("data");
                for(int i=0; i < items.length(); i++){
                    JSONObject news = items.getJSONObject(i);
                    String t = news.getString("title");
                    String c = news.getString("content");
                    textView.append(t+"\n");
                    textView.append(c+"\n");
                    textView.append("--------------\n\n");
                }
            }else{
                Toast.makeText(context, "Data is not retrieved", Toast.LENGTH_SHORT).show();
            }
        }catch (Exception e){
            e.printStackTrace();
        }*/

        progressBar.setVisibility(View.GONE);
        Gson gson = new Gson();
        News news = gson.fromJson(s,News.class);
        NewsAdapter newsAdapter = new NewsAdapter(context,news);
        recyclerView.setAdapter(newsAdapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(context));

        Toast.makeText(context, news.getData().get(0).getTitle(), Toast.LENGTH_SHORT).show();
    }
}
