package com.nareshittechnologies.newsinshort;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;

public class NewsAdapter extends RecyclerView.Adapter<NewsAdapter.NewsViewHolder> {

    Context context;
    News news;

    public NewsAdapter(Context context, News news) {
        this.context = context;
        this.news = news;
    }

    @NonNull
    @Override
    public NewsViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(context).inflate(R.layout.one_item_design,parent,false);
        return new NewsViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull NewsViewHolder holder, int position) {
        // We will see loading of image later
        holder.news_title.setText(news.getData().get(position).getTitle());
        Glide.with(context).load(news.getData().get(position).getImageUrl()).into(holder.news_poster);
    }

    @Override
    public int getItemCount() {
        return news.getData().size();
    }

    public class NewsViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        ImageView news_poster;
        TextView news_title;
        public NewsViewHolder(@NonNull View itemView) {
            super(itemView);
            news_poster = itemView.findViewById(R.id.news_poster);
            news_title = itemView.findViewById(R.id.news_title);

            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            int pos = getAdapterPosition();
            Datum datum = news.getData().get(pos);
            Intent i = new Intent(context,DetailsScreen.class);
            i.putExtra("KEY",datum);
            context.startActivity(i);
        }
    }
}
