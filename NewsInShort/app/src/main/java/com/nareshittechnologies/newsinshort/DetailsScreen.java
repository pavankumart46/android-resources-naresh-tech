package com.nareshittechnologies.newsinshort;

import androidx.appcompat.app.AppCompatActivity;
import androidx.browser.customtabs.CustomTabColorSchemeParams;
import androidx.browser.customtabs.CustomTabsIntent;

import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

public class DetailsScreen extends AppCompatActivity {

    ImageView details_image;
    TextView details_title, details_content;
    Datum datum;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_details_screen);

        details_image = findViewById(R.id.news_image_detail);
        details_title = findViewById(R.id.news_title_detail);
        details_content = findViewById(R.id.news_content_details);

        Intent intent = getIntent();
        datum = (Datum) intent.getSerializableExtra("KEY");

        Glide.with(this).load(datum.getImageUrl()).into(details_image);

        details_title.setText(datum.getTitle());
        details_content.setText(datum.getContent());
    }

    public void openLink(View view){
    int colorInt = Color.parseColor("#0000FF"); //red
    CustomTabColorSchemeParams defaultColors = new CustomTabColorSchemeParams.Builder()
            .setToolbarColor(colorInt)
            .build();

        String url = datum.getUrl();
        CustomTabsIntent.Builder builder = new CustomTabsIntent.Builder();
        builder.setDefaultColorSchemeParams(defaultColors);
        CustomTabsIntent customTabsIntent = builder.build();
        customTabsIntent.launchUrl(this, Uri.parse(url));
    }
}