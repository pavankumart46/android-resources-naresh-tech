package com.nareshittechnologies.musicplayer;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.view.View;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

    }

    public void playSong(View view) {
        Intent i = new Intent(this,MediaService.class);
        startService(i);
    }

    public void stopSong(View view) {
        Intent i = new Intent(this,MediaService.class);
        stopService(i);
    }
}