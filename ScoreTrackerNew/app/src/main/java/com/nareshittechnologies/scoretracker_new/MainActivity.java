package com.nareshittechnologies.scoretracker_new;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;

import android.os.Bundle;
import android.view.View;

import com.nareshittechnologies.scoretracker_new.databinding.ActivityMainBinding;

public class MainActivity extends AppCompatActivity {

    // ActivityMainBinding is generated because you enabled the databinding library and also
    // encapsulated the layout file (activity_main.xml) with layout Tag
    // if the xml file name is activity_second.xml -> ActivitySecondBinding.java will be generated.
    ActivityMainBinding binding;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this,R.layout.activity_main);
        binding.setCount(0);
    }

    public void minusScore(View view) {
        binding.setCount(binding.getCount() - 1);
    }

    public void plusScore(View view) {
        binding.setCount(binding.getCount() + 1);
    }
}