package com.nareshittechnologies.favoritefood;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.StaggeredGridLayoutManager;

import android.os.Bundle;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    List<String> fav_food_names;
    List<Integer> fav_food_images;
    List<String> fav_food_main_ingredients;

    RecyclerView recyclerView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        recyclerView = findViewById(R.id.recyclerview);
        prepareData();

        FavFoodAdapter adapter = new FavFoodAdapter(this,fav_food_names,fav_food_main_ingredients,fav_food_images);
        recyclerView.setAdapter(adapter);

        recyclerView.setLayoutManager(new LinearLayoutManager(this));
    }

    // Todo 1: Preparing data
    private void prepareData() {
        fav_food_names = new ArrayList<>();
        fav_food_images = new ArrayList<>();
        fav_food_main_ingredients = new ArrayList<>();

        fav_food_images.add(R.drawable.dabeli);
        fav_food_names.add("DABELI");
        fav_food_main_ingredients.add("Pav, Masala, Potato");

        fav_food_images.add(R.drawable.dosa);
        fav_food_names.add("DOSA");
        fav_food_main_ingredients.add("Black Gram & Rice Batter, Coconuts");

        fav_food_images.add(R.drawable.panipuri);
        fav_food_names.add("Pani Puri");
        fav_food_main_ingredients.add("Pani, Puri, Potato");

        fav_food_images.add(R.drawable.poori);
        fav_food_names.add("Poori");
        fav_food_main_ingredients.add("Wheat Flour & Maida, Masala, Potato");

        fav_food_images.add(R.drawable.tandoori);
        fav_food_names.add("TANDOORI");
        fav_food_main_ingredients.add("Whole Chicken, Masala");

        fav_food_images.add(R.drawable.pulihora);
        fav_food_names.add("PULIHORA");
        fav_food_main_ingredients.add("Cooked Rice, Tamrind, Chillies");

        fav_food_images.add(R.drawable.bonda);
        fav_food_names.add("BONDA");
        fav_food_main_ingredients.add("Maida batter, Oil");

        fav_food_images.add(R.drawable.sambaridly);
        fav_food_names.add("SAMBAR IDLY");
        fav_food_main_ingredients.add("Black Gram Batter, Sambar");

        fav_food_images.add(R.drawable.upma);
        fav_food_names.add("UPMA");
        fav_food_main_ingredients.add("Ravva, Onions, Chillies");

        fav_food_images.add(R.drawable.vada);
        fav_food_names.add("VADA");
        fav_food_main_ingredients.add("Black Gram Batter, oil");
    }

    // Todo 2: Add RecyclerView to the project

    // TODO 3: Design for one item - Which eventually be applied for all items in the recyclerview

    // TODO 4: Create a RecyclerView.Adapter class - Provide data and Recyclerview to it
    //  so that the adapter can populate the data on the recyclerview
    //  This Adapter should also include a View Holder

    // TODO 5: set up the Adapter on top of the Recyclerview

    // TODO 6: set up a Layout Manager to Arrange the data items in whatever the way you want.
}