package com.nareshittechnologies.favoritefood;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.snackbar.BaseTransientBottomBar;
import com.google.android.material.snackbar.Snackbar;

import java.util.List;

public class FavFoodAdapter extends RecyclerView.Adapter<FavFoodAdapter.FavFoodViewHolder> {

    // TODO 4.1 - First get the data & Context - use a Constructor to simplify the process
    Context context;
    List<String> fav_food_names, fav_food_ingredients;
    List<Integer> fav_food_images;

    public FavFoodAdapter(Context context, List<String> fav_food_names, List<String> fav_food_ingredients, List<Integer> fav_food_images) {
        this.context = context;
        this.fav_food_names = fav_food_names;
        this.fav_food_ingredients = fav_food_ingredients;
        this.fav_food_images = fav_food_images;
    }

    // onCreateViewHolder helps us to inflate (attach) the one_item_design.xml to all the items
    // on the recyclerview
    // Return Type - an Object of ViewHolder class (FavFoodViewHolder)
    @NonNull
    @Override
    public FavFoodViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.one_item_design,parent,false);
        return new FavFoodViewHolder(view);
    }

    // onBindViewHolder(...) is responsible to populate data properly based on the position
    // on the recyclerview
    @Override
    public void onBindViewHolder(@NonNull FavFoodViewHolder holder, int position) {
        holder.fav_images.setImageResource(fav_food_images.get(position));
        holder.fav_names.setText(fav_food_names.get(position));
        holder.fav_ingred.setText(fav_food_ingredients.get(position));
    }

    // Returns the total number of items that will be displayed on Recyclerview
    @Override
    public int getItemCount() {
        return fav_food_images.size();
    }

    // view holder class will connect the views on one_item_design.xml with  Java Objects
    public class FavFoodViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        ImageView fav_images;
        TextView fav_names, fav_ingred;
        public FavFoodViewHolder(@NonNull View itemView) {
            super(itemView);

            fav_images = itemView.findViewById(R.id.fav_img);
            fav_names = itemView.findViewById(R.id.fav_name);
            fav_ingred = itemView.findViewById(R.id.fav_ingred);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            int pos = getAdapterPosition();

            Snackbar snackbar = Snackbar.make(view,fav_food_names.get(pos), BaseTransientBottomBar.LENGTH_INDEFINITE);
            snackbar.setAction(R.string.action_snackbar, new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    snackbar.dismiss();
                }
            });
            snackbar.show();
        }
    }
}
