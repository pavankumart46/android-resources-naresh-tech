package com.nareshittechnologies.scoretracker_new;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;

import android.os.Bundle;
import android.view.View;

import com.nareshittechnologies.scoretracker_new.databinding.ActivityMainBinding;

public class MainActivity extends AppCompatActivity {

    // ActivityMainBinding is generated because you enabled the databinding library and also
    // encapsulated the layout file (activity_main.xml) with layout Tag
    // if the xml file name is activity_second.xml -> ActivitySecondBinding.java will be generated.
    ActivityMainBinding binding;
    MainViewModel vm;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this,R.layout.activity_main);
        vm = new ViewModelProvider(this).get(MainViewModel.class);
        vm.count.observe(this, new Observer<Integer>() {
            @Override
            public void onChanged(Integer integer) {
                binding.result.setText(String.valueOf(integer));
            }
        });
    }

    public void minusScore(View view) {
        vm.decrement();
    }

    public void plusScore(View view) {
        vm.increment();
    }
}