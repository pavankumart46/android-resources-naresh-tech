package com.nareshittechnologies.scoretracker_new;

import android.util.Log;

import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import java.nio.charset.MalformedInputException;

public class MainViewModel extends ViewModel {

    // viewmodel is created in the constructor

    MutableLiveData<Integer> count;
    public MainViewModel() {
        Log.d("MAIN","View Model is created!");
        count = new MutableLiveData<>();
        count.setValue(0);
    }

    // ViewModel is destroyed in the oncleared() method

    @Override
    protected void onCleared() {
        super.onCleared();
        Log.d("MAIN","View Model is destroyed!");
    }

    public void increment(){
        count.setValue(count.getValue() + 1);
    }

    public void decrement(){
        count.setValue(count.getValue() - 1);
    }
}
