package com.nareshittechnologies.tabnavigation;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;
import androidx.viewpager.widget.ViewPager;
import androidx.viewpager2.widget.ViewPager2;

import android.os.Bundle;

import com.google.android.material.tabs.TabLayout;

public class MainActivity extends AppCompatActivity {

    ViewPager vp;
    TabLayout tl;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        vp = findViewById(R.id.viewpager);
        tl = findViewById(R.id.tablayout);
        ViewPagerAdapter vpa = new ViewPagerAdapter(getSupportFragmentManager());
        vp.setAdapter(vpa);
        // Setting up tablayout in sync with viewpager
        tl.setupWithViewPager(vp);
    }

    // In Android, we cannot display an activity on top of another activity
    // We have fragments to help us with this kind of approach

    // TODO: Create three different Fragments (Screens) and display them on MainActivity
    //  with the help of a ViewPager

    class ViewPagerAdapter extends FragmentStatePagerAdapter{

        public ViewPagerAdapter(@NonNull FragmentManager fm) {
            super(fm);
        }

        @NonNull
        @Override
        public Fragment getItem(int position) {
            if(position == 0){
                return new RedFragment();
            }else if(position == 1){
                return new GreenFragment();
            }else if(position == 2){
                return new BlueFragment();
            }
            return null;
        }

        @Override
        public int getCount() {
            return 3;
        }

        @Nullable
        @Override
        public CharSequence getPageTitle(int position) {
            switch (position){
                case 0: return "Red";
                case 1: return "Green";
                case 2: return "Blue";
            }
            return super.getPageTitle(position);
        }
    }
}