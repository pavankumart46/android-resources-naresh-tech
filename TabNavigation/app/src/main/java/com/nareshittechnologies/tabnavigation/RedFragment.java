package com.nareshittechnologies.tabnavigation;

import android.os.Bundle;
import androidx.fragment.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class RedFragment extends Fragment {

    EditText n1, n2;
    TextView result;
    Button plusBtn;

    public RedFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view =  inflater.inflate(R.layout.fragment_red, container, false);
        n1 = view.findViewById(R.id.number1);
        n2 = view.findViewById(R.id.number2);
        result = view.findViewById(R.id.result);
        plusBtn = view.findViewById(R.id.plusBtn);

        plusBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int x = Integer.parseInt(n1.getText().toString());
                int y = Integer.parseInt(n2.getText().toString());
                int z = x+y;
                result.setText(String.valueOf(z));
            }
        });

        return view;
    }
}