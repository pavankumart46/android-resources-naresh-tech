package com.nareshittechnologies.activitylifecycle;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.util.Log;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Log.d("MAIN","onCreate() method");
    }

    @Override
    protected void onStart() {
        super.onStart();
        Log.d("MAIN","onStart() method");
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.d("MAIN","onResume() method");
    }

    // Now after executing oncreate(), onstart(...) and onResume(...)
    // your activity is in Interactable state [Activity Running]


    @Override
    protected void onPause() {
        super.onPause();
        Log.d("MAIN","onPause() method");
    }

    @Override
    protected void onStop() {
        super.onStop();
        Log.d("MAIN","onStop() method");
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Log.d("MAIN","onDestroy() method");
    }

    // [Activity ShutDown] The activity will no longer be visible nor does it occupy
    // Any space in the system.


    @Override
    protected void onRestart() {
        super.onRestart();
        Log.d("MAIN","onRestart() method");
    }
}