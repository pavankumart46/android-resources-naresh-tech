package com.nareshittechnologies.contentreceiver;

import androidx.appcompat.app.AppCompatActivity;

import android.content.ContentValues;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    // Prefix "content://" before the URI Authority - So that the system will understand
    // that you are trying to access a content provider for data
    String authorities = "content://com.nareshittechnologies.sqlitedatabase.CONTENT_PROVIDER";
    TextView textView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        textView = findViewById(R.id.data_textview);
    }

    public void loadData(View view) {
        Uri CONTENT_URI = Uri.parse(authorities);
        textView.setText("");
        Cursor c = getContentResolver().query(CONTENT_URI,null,null,null,null,null);
        if(c!=null){
            c.moveToFirst();
            do{
                int student_id = c.getInt(0);
                String student_name = c.getString(1);
                int age = c.getInt(2);
                textView.append(student_id+":"+student_name+":"+age+"\n");
            }while(c.moveToNext());
        }else{
            Toast.makeText(this, "Cursor is null!", Toast.LENGTH_SHORT).show();
        }

    }

    public void saveData(View view) {
        EditText name = findViewById(R.id.editTextTextPersonName);
        EditText age = findViewById(R.id.editTextTextPersonName2);
        String n = name.getText().toString();
        int a = Integer.parseInt(age.getText().toString());

        ContentValues values = new ContentValues();
        values.put("stu_name",n);
        values.put("stu_age",a);
        
        Uri CONTENT_URI = Uri.parse(authorities);
        getContentResolver().insert(CONTENT_URI,values);
        Toast.makeText(this, "DATA is inserted!", Toast.LENGTH_SHORT).show();
    }
    /*
    * in the background when you call the content provider - the provider will be running
    * on a separate thread - Make sure that you are not having any code that accesses ui toolkit
    * */
}