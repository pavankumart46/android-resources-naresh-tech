package com.nareshittechnologies.freedictionary;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.text.Layout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;
import com.nareshittechnologies.freedictionary.Models.DataSource;
import com.nareshittechnologies.freedictionary.Models.Definition;
import com.nareshittechnologies.freedictionary.Models.Meaning;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

public class MainActivity extends AppCompatActivity {

    private EditText editText;
    private RecyclerView recyclerView;
    private ProgressBar progressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        editText = findViewById(R.id.word_entry);
        recyclerView = findViewById(R.id.recyclerview);
        progressBar = findViewById(R.id.progressBar);
        progressBar.setVisibility(View.GONE);

    }

    public void searchMeaning(View view) {
        if(editText.getText().toString().equalsIgnoreCase("")){
            editText.setError("PLEASE ENTER A WORD TO SEARCH");
        }else{
            progressBar.setVisibility(View.VISIBLE);
            String word = editText.getText().toString();
            String url = "https://api.dictionaryapi.dev/api/v2/entries/en/"+word;

            RequestQueue queue = Volley.newRequestQueue(this);
            StringRequest request = new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    progressBar.setVisibility(View.GONE);
                    Gson gson = new Gson();
                    DataSource[] dataSources = gson.fromJson(response,DataSource[].class);
                    Toast.makeText(MainActivity.this, dataSources[0].getWord(), Toast.LENGTH_SHORT).show();
                    WordAdapter wordAdapter = new WordAdapter(dataSources);
                    recyclerView.setAdapter(wordAdapter);
                    recyclerView.setLayoutManager(new LinearLayoutManager(MainActivity.this));
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    progressBar.setVisibility(View.GONE);
                    Toast.makeText(MainActivity.this, "Did not fetch data!", Toast.LENGTH_SHORT).show();
                }
            });

            queue.add(request);
        }
    }

    class WordAdapter extends RecyclerView.Adapter<WordAdapter.WordViewHolder>{

        DataSource[] dataSources;

        public WordAdapter(DataSource[] dataSources) {
            this.dataSources = dataSources;
        }

        @NonNull
        @Override
        public WordViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            return new WordViewHolder(LayoutInflater.from(MainActivity.this).inflate(R.layout.one_item_design,
                    parent,false));
        }

        @Override
        public void onBindViewHolder(@NonNull WordViewHolder holder, int position) {
            DataSource dataSource = dataSources[position];
            String w = dataSource.getWord();
            List<Meaning> list = dataSource.getMeanings();
            StringBuilder stringBuilder = new StringBuilder();
            for(Meaning meaning: list){
                List<Definition> definitions = meaning.getDefinitions();
                for(Definition definition:definitions){
                    stringBuilder.append(definition.getDefinition()+"\n\n");
                }
            }
            holder.wordName.setText(w.toUpperCase());
            holder.wordMeanings.setText(stringBuilder.toString());
        }

        @Override
        public int getItemCount() {
            return dataSources.length;
        }

        public class WordViewHolder extends RecyclerView.ViewHolder {
            TextView wordName, wordMeanings;
            public WordViewHolder(@NonNull View itemView) {
                super(itemView);
                wordName = itemView.findViewById(R.id.word_tv);
                wordMeanings = itemView.findViewById(R.id.textView2);
            }
        }
    }
}