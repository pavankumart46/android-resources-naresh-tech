
package com.nareshittechnologies.freedictionary.Models;

import java.util.List;


public class DataSource {

    private String word;
    private String phonetic;
    private List<Phonetic> phonetics = null;
    private List<Meaning> meanings = null;
    private License__1 license;
    private List<String> sourceUrls = null;

    public String getWord() {
        return word;
    }

    public void setWord(String word) {
        this.word = word;
    }

    public String getPhonetic() {
        return phonetic;
    }

    public void setPhonetic(String phonetic) {
        this.phonetic = phonetic;
    }

    public List<Phonetic> getPhonetics() {
        return phonetics;
    }

    public void setPhonetics(List<Phonetic> phonetics) {
        this.phonetics = phonetics;
    }

    public List<Meaning> getMeanings() {
        return meanings;
    }

    public void setMeanings(List<Meaning> meanings) {
        this.meanings = meanings;
    }

    public License__1 getLicense() {
        return license;
    }

    public void setLicense(License__1 license) {
        this.license = license;
    }

    public List<String> getSourceUrls() {
        return sourceUrls;
    }

    public void setSourceUrls(List<String> sourceUrls) {
        this.sourceUrls = sourceUrls;
    }

}
