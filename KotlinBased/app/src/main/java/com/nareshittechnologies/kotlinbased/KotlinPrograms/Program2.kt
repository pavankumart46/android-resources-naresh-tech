package com.nareshittechnologies.kotlinbased.KotlinPrograms


open class MyClass{
    fun classFun(){
        println("INSIDE CLASS AND FUNCTION")
    }
}

class MyClass2 : MyClass() {
    fun classFun2(){
        println("This is another class fun")
    }
}

fun main(){
    val obj = MyClass2()
    obj.classFun()
    obj.classFun2()
}