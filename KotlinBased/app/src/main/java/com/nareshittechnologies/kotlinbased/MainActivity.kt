package com.nareshittechnologies.kotlinbased

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.EditText
import android.widget.TextView

class MainActivity : AppCompatActivity(){
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
    }

    fun addTwo(view: View) {
        val e:EditText = findViewById(R.id.num1_et);
        val e2:EditText = findViewById(R.id.num2_et);
        val a:Int = e.text.toString().toInt();
        val b:Int = e2.text.toString().toInt();
        val c = a+b
        val t:TextView = findViewById(R.id.textView)
        t.text = c.toString()
    }
}