package com.nareshittechnologies.kotlinbased.KotlinPrograms

/**
 * This is documentation comment
 * that usually includes many lines of text*/
fun main(){
    println("Hello World!");
    println(sum(10,20));
    val a = 20;
    val b:Int = 30;
    val greatest = if(a>b) a else b
    println(greatest)
}

fun sum(a:Int, b:Int):Int{
    return a+b
}
