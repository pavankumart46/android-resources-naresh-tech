package com.nareshittechnologies.explicitintents;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

public class SecondActivity extends AppCompatActivity {

    TextView tv;
    ImageView imageView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_second);
        tv = findViewById(R.id.textView);
        imageView = findViewById(R.id.imageView);
        Intent in = getIntent();
        String data = in.getStringExtra("KEY");
        tv.setText(data);

        int img_id = in.getIntExtra("IMAGE",R.drawable.ic_launcher_background);
        imageView.setImageResource(img_id);
    }
}