package com.nareshittechnologies.explicitintents;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void openNext(View view) {
        // TODO 1: Create an Intent object with source & destination passed to the Constructor
        Intent i = new Intent(this,SecondActivity.class);
        // TODO 1.1 if you want to send data, attach it to the intnet object
        i.putExtra("KEY","PAVAN");
        i.putExtra("IMAGE",R.drawable.hello);
        // TODO 2: start the activity with the help of startActivity(...)
        startActivity(i);
    }
}