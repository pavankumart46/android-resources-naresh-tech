package com.nareshittechnologies.passwordsaver;

import androidx.appcompat.app.AppCompatActivity;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    private EditText username, password;
    private TextView result;
    private SharedPreferences preferences;
    SharedPreferences.OnSharedPreferenceChangeListener listener;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        username = findViewById(R.id.username);
        password = findViewById(R.id.password);
        result = findViewById(R.id.textView);

        // Creating a Shared Preferences File
        preferences = getSharedPreferences("password_saver",MODE_PRIVATE);

        listener = new SharedPreferences.OnSharedPreferenceChangeListener() {
            @Override
            public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String s) {
                String one = preferences.getString("USER_NAME","NO USER NAME FOUND");
                String pass = preferences.getString("PASS","NO PASSWORD");

                result.setText(one+"\n"+pass);
            }
        };

        preferences.registerOnSharedPreferenceChangeListener(listener);
    }

    public void saveData(View view) {
        // TODO 1: save the data into shared preferences file
        String un = username.getText().toString();
        String p = password.getText().toString();

        SharedPreferences.Editor editor = preferences.edit();
        editor.putString("USER_NAME",un);
        editor.putString("PASS",p);
        editor.apply(); // or you can also call editor.commit() to save the changes into the SharedPreferences file

        Toast.makeText(this, "DATA SAVED SUCCESSFULLY", Toast.LENGTH_SHORT).show();

        username.getText().clear();
        password.getText().clear();
    }

    @Override
    protected void onPause() {
        super.onPause();
        preferences.unregisterOnSharedPreferenceChangeListener(listener);
    }

    /*public void loadData(View view) {
        // TODO 2: load the data of the shared preferences file
        String one = preferences.getString("USER_NAME","NO USER NAME FOUND");
        String pass = preferences.getString("PASS","NO PASSWORD");

        result.setText(one+"\n"+pass);
    }*/

    // When you try adding data to the shared preferences file, do it in key and value pairs
}