package com.nareshittechnologies.animationsinandroid;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.ImageView;

public class MainActivity extends AppCompatActivity {

    ImageView imageView;
    Button b;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        imageView = findViewById(R.id.imageView);
        b = findViewById(R.id.button);
    }

    public void blinkAnimation(View view) {
        Animation anim = AnimationUtils.loadAnimation(this,R.anim.blink_animation);
        imageView.startAnimation(anim);
    }

    public void fadeOut(View view) {
        Animation anim = AnimationUtils.loadAnimation(this,R.anim.fade_animation);
        imageView.startAnimation(anim);
    }

    public void move(View view) {
        Animation anim = AnimationUtils.loadAnimation(this,R.anim.move_animation);
        imageView.startAnimation(anim);
    }

    public void rotate(View view) {
        Animation anim = AnimationUtils.loadAnimation(this,R.anim.rotate_animation);
        imageView.startAnimation(anim);
    }
}