package com.nareshittechnologies.externalstorage;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.Manifest;
import android.content.pm.PackageManager;
import android.os.Binder;
import android.os.Bundle;
import android.os.Environment;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.InputStreamReader;

public class MainActivity extends AppCompatActivity {

    EditText name, age;
    TextView result;
    private static final String FILE_NAME = "Naresh_Tech.txt";


    boolean isAvailable = false;
    boolean isWritable = false;
    boolean isReadable = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        name = findViewById(R.id.et_name);
        age = findViewById(R.id.et_age);
        result = findViewById(R.id.textView);

        if(checkCallingOrSelfPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED
        && checkCallingOrSelfPermission(Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED){
            Toast.makeText(this, "PERMISSIONS ARE GRANTED", Toast.LENGTH_SHORT).show();
        }else{
            requestPermissions(new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE,
                    Manifest.permission.READ_EXTERNAL_STORAGE},234);
        }

        String state = Environment.getExternalStorageState();
        // MEDIA_MOUNTED - if state contains MEDIA_MOUNTED -> storage is available, readable and writable
        // MEDIA_MOUNTED_READ_ONLY - if state contains this String -> storage is available and readable but not writable.
        if(Environment.MEDIA_MOUNTED.equals(state)){
            isAvailable = isReadable = isWritable = true;
        }else if(Environment.MEDIA_MOUNTED_READ_ONLY.equals(state)){
            isAvailable = isReadable = true;
            isWritable = false;
        }
    }


    public void loadData(View view) {
        File folder = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS);
        File file = new File(folder,FILE_NAME);
        FileInputStream fis;
        try {
            fis = new FileInputStream(file);
            BufferedReader br = new BufferedReader(new InputStreamReader(fis));
            String line = "";
            StringBuffer sb = new StringBuffer();
            while((line = br.readLine())!=null){
                sb.append(line);
            }
            result.setText(sb.toString());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void saveData(View view) {
        if(isAvailable && isWritable){
            // You have to really check if the write to external storage permission is granted or not
            // STEP 1: Select a public folder
            File folder = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS);
            // STEP 2: Store data after creating the file
            File file = new File(folder,FILE_NAME);
            FileOutputStream fos;
            try {
                fos = new FileOutputStream(file);
                fos.write("RANDOM".getBytes());
                fos.close();
                Toast.makeText(this, "DATA is Saved", Toast.LENGTH_SHORT).show();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        for(int i = 0; i<permissions.length; i++){
            if(grantResults[i] == PackageManager.PERMISSION_GRANTED){
                Toast.makeText(this, permissions[i] +" Granted", Toast.LENGTH_SHORT).show();
            }
        }
    }
}