##### Reach out to me on ```pavankreddy.t@gmail.com``` for any of your doubts or Concerns

### Project Structure in Android
#### [Read more about Project Structure Here](https://developer.android.com/studio/projects)

### Options Menu in android
### [Read about menu](https://developer.android.com/guide/topics/ui/menus)

#### Assignment
- Go through Common Intents [Documentation](https://developer.android.com/guide/components/intents-common) and pick up two common itents and do an application!

#### Layouts, Views and Viewgroups
> *View - What ever the UI Components you see on an Android App is called a VIEW. EX: Buttons, EditText, Slider, RadioButton, etc.,
> *ViewGroup - A ViewGroup is a View that can accommodate other Views and ViewGroups inside it. 
> *Layouts - Layouts gives us the structure to organize the Views of our Screen - all Layouts are ViewGroups. The Root view is always a ViewGroup.

#### Desiging the user interface
1. We use SP as the units for changing the textSize - SP - Scalable Pixels.
2. We use DP as the units for setting the dimensions for thee Views - DP- Display Pixels.

#### Connecting the textview to the java file (object reference - findviewbyId() method)
#### How to set the text on the TextView using settext() method ?
#### How to Read the documentation related to [TextView](https://developer.android.com/reference/android/widget/TextView)
#### How to Check error Log using logCat?
#### Alternatives to Button's onClick attribute. (SetOnClickListner() method and using OnClickListener() Interface)

#### What are activities?
#### How to create an activity?
#### Intents
#### Types of Intents
	-	 Explicit Intents
	- 	 Implicit Intents

#### [For Presentation that is used to explain intents and activities Click Here](https://docs.google.com/presentation/d/1kjxsI9brdVRIx3rqoB0H-1-PmVlzJbiQNf4PyqzZKJM/edit#slide=id.g181baec304_0_0)

#### [Common Intents](https://developer.android.com/guide/components/intents-common)
	- Accessing Web Browser
	- Opening Maps
	- Opening Wifi Settings
	- Using Camera on the device
### Assignment 1: Use Set Alarm Common Intent to set an alarm for a specific time

#### [Activity LifeCycle](https://developer.android.com/guide/components/activities/activity-lifecycle)
#### [Saving the Instance State of an Activity](https://developer.android.com/topic/libraries/architecture/saving-states)
##### [Example - Explained](ScoreTracker/app/src/main/java/com/nareshit/scoretracker)
#### [ScrollView in Android](https://developer.android.com/reference/android/widget/ScrollView)

#### [Managing the String Resources](ScoreTracker/app/src/main/res/values/strings.xml)

#### [RadioButtons](https://developer.android.com/guide/topics/ui/controls/radiobutton)
#### [Spinner](https://developer.android.com/guide/topics/ui/controls/spinner)

#### [Menus in Android](https://developer.android.com/guide/topics/ui/menus)
#### How to create Menu XML file ?
	- rightclick on RES > new >Android Resource file> 
	- in the pop up window> give resource file name and then choose menu as the value type.
	- Menu tag will be available by default, you can add items as per your requirement.
#### Options Menu
#### Context Menu
#### Popup Menu
#### Example - Score Tracker Application

#### [Constraint Layout](https://developer.android.com/training/constraint-layout)


#### [List View](https://developer.android.com/reference/android/widget/ListView)

#### [RecyclerView - PRESENTATION](https://docs.google.com/presentation/d/1nFJqH0OSSZmjaycRzEGE6vvsm6jlxghQyoO15KKbkwc/edit?usp=sharing)
#### [RecyclerView - Official Documentation](https://developer.android.com/guide/topics/ui/layout/recyclerview?gclid=Cj0KCQiAsqOMBhDFARIsAFBTN3cRVPR8cRokxjBGYPfKRY5u03SyTHB2Xj_tutmT6EaYENc9_TgsCKgaAmQ8EALw_wcB&gclsrc=aw.ds)

#### [Pickers in Android](https://developer.android.com/guide/topics/ui/controls/pickers#:~:text=Android%20provides%20controls%20for%20the,month%2C%20day%2C%20year)
#### [Tab Navigation](https://developer.android.com/guide/navigation/navigation-swipe-view)
#### [AsyncTask](https://developer.android.com/reference/android/os/AsyncTask)

#### Async Task and Glide Library
#### [Async Task and Recyclerview](https://developer.android.com/reference/android/os/AsyncTask)

#### [Boradcast Receivers](https://developer.android.com/guide/components/broadcasts)
#### [Notifications In Android](https://developer.android.com/guide/topics/ui/notifiers/notifications)

#### [Alarm Managers](https://developer.android.com/reference/android/app/AlarmManager)
#### [Shared Preferences](https://developer.android.com/training/data-storage/shared-preferences)
#### [JobSchedulers](https://developer.android.com/reference/android/app/job/JobScheduler)

#### [Permissions](https://developer.android.com/guide/topics/permissions/overview)

#### [Services in Android](https://developer.android.com/guide/components/services)

#### [Media Player](https://developer.android.com/reference/android/media/MediaPlayer)

#### [Data Binding Library - Newest replacement of the deprecated ButterKnife library](https://developer.android.com/topic/libraries/data-binding)


#### [View model](https://developer.android.com/topic/libraries/architecture/viewmodel?gclid=Cj0KCQiAzfuNBhCGARIsAD1nu--B6TIZHjrlm6sYcYPe49uE2qdfC30AaNTVZFrd3IwfnmKwp9It8JMaAqnQEALw_wcB&gclsrc=aw.ds#codelab)

#### [Room Database](https://developer.android.com/jetpack/androidx/releases/room?gclid=Cj0KCQiAk4aOBhCTARIsAFWFP9HNxj8t6q9dpezcYE4BEn8VIfH4mG3G9zg-BdOUrFRSsVqioqVV0DoaAotbEALw_wcB&gclsrc=aw.ds)

#### [Firebase Real Time data bases and other related docs](https://firebase.google.com/docs/database/)

#### [Google Maps](https://developers.google.com/maps/documentation/)

#### [Google Places API Integration - Nearby Places](https://developers.google.com/maps/documentation/places/web-service/overview)

#### [Google AdMob Account](https://developers.google.com/admob/) - Hit on Android icon to get Started

#### [Learn Kotlin Programming here](https://kotlinlang.org/)

#### [Android Kotlin Developer Fundamentals Course (Self learn - Self paced) course by Google](https://developer.android.com/courses/kotlin-android-fundamentals/overview)
