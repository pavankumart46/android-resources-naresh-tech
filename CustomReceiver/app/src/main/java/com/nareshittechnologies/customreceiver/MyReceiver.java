package com.nareshittechnologies.customreceiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.widget.Toast;

public class MyReceiver extends BroadcastReceiver {

    public static final String CustomBroadcast = "com.nareshittechnologies.powerreceiverapp.CUSTOM_BROADCAST";

    @Override
    public void onReceive(Context context, Intent intent) {
        // TODO: This method is called when the BroadcastReceiver is receiving
        // an Intent broadcast.
        if(intent.getAction() == CustomBroadcast){
            Toast.makeText(context, "RECEIVED", Toast.LENGTH_SHORT).show();
        }
    }
}