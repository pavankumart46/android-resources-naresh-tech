package com.nareshittechnologies.roomdatabaseexample;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;

import java.util.List;

@Dao
public interface StudentDAO {

    @Insert
    void insertData(Students students);

    @Query("select * from students")
    List<Students> getData();

}

/**
 * DAO - Data Access Object*/
