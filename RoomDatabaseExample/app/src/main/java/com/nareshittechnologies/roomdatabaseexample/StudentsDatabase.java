package com.nareshittechnologies.roomdatabaseexample;

import androidx.room.Database;
import androidx.room.RoomDatabase;

@Database(entities = {Students.class},version = 1)
public abstract class StudentsDatabase extends RoomDatabase {
    public abstract StudentDAO studentDAO();
}
/**
 * To Perform Database related operations, we will interact with this class.*/