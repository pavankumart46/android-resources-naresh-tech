package com.nareshittechnologies.roomdatabaseexample;

import androidx.appcompat.app.AppCompatActivity;
import androidx.room.Room;

import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.util.List;

public class MainActivity extends AppCompatActivity {

    EditText name, age;
    TextView result;
    StudentsDatabase database;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        name = findViewById(R.id.et_name);
        age = findViewById(R.id.et_age);
        result = findViewById(R.id.result);

        database = Room.databaseBuilder(this,StudentsDatabase.class,"college")
                .allowMainThreadQueries()
                .build();
    }

    public void saveData(View view) {
        // Have the save the data in room database
        String n = name.getText().toString();
        int a = Integer.parseInt(age.getText().toString());
        Students s = new Students(n,a);
        database.studentDAO().insertData(s);
        Toast.makeText(this, "Data Inserted Successfully", Toast.LENGTH_SHORT).show();
    }

    public void loadData(View view) {
        List<Students> list = database.studentDAO().getData();
        result.setText("");
        for(Students s: list){
            result.append(s.getStudent_name()+" "+s.getStudent_age()+"\n");
        }
    }
}