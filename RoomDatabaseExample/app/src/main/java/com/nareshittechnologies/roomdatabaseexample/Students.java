package com.nareshittechnologies.roomdatabaseexample;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

// Class Name -> Table Name
// Variables names -> coloumn names

@Entity
public class Students {

    @PrimaryKey(autoGenerate = true)
    int student_id;

    String student_name;

    int student_age;

    public Students(String student_name, int student_age) {
        this.student_name = student_name;
        this.student_age = student_age;
    }

    public int getStudent_id() {
        return student_id;
    }

    public void setStudent_id(int student_id) {
        this.student_id = student_id;
    }

    public String getStudent_name() {
        return student_name;
    }

    public void setStudent_name(String student_name) {
        this.student_name = student_name;
    }

    public int getStudent_age() {
        return student_age;
    }

    public void setStudent_age(int student_age) {
        this.student_age = student_age;
    }
}
