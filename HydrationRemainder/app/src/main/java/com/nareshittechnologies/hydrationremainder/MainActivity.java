package com.nareshittechnologies.hydrationremainder;

import androidx.appcompat.app.AppCompatActivity;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Intent;
import android.os.Bundle;
import android.os.SystemClock;
import android.widget.CompoundButton;
import android.widget.Switch;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    AlarmManager manager;
    Switch hydrationRemainder;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        hydrationRemainder = findViewById(R.id.switch1);
        manager = (AlarmManager) getSystemService(ALARM_SERVICE);
        Intent i = new Intent(MainActivity.this, MyReceiver.class);
        PendingIntent pi = PendingIntent.getBroadcast(MainActivity.this,23,i,PendingIntent.FLAG_UPDATE_CURRENT);
        hydrationRemainder.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if(b){
                    // Switch is turned on
                    manager.setInexactRepeating(AlarmManager.ELAPSED_REALTIME,
                            SystemClock.elapsedRealtime(),
                            2*60*1000,
                            pi);
                    Toast.makeText(MainActivity.this, "Alarm Turned on", Toast.LENGTH_SHORT).show();
                }else{
                    // Switch is turned off
                    manager.cancel(pi);
                    Toast.makeText(MainActivity.this, "Alarm Turned Off", Toast.LENGTH_SHORT).show();
                }
            }
        });





    }
}