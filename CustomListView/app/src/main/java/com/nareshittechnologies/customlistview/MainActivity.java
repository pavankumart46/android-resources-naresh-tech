package com.nareshittechnologies.customlistview;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    ListView mListView;
    String[] items = new String[]{"Apple","Banana","Cat","dog","Eagle"};
    int[] images = new int[]{R.drawable.apple,R.drawable.banana,R.drawable.cat,R.drawable.dog,R.drawable.eagle};
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mListView = findViewById(R.id.listview);

        MyAdapter myAdapter = new MyAdapter();
        mListView.setAdapter(myAdapter);
    }

    class MyAdapter extends BaseAdapter{

        @Override
        public int getCount() {
            return items.length;
        }

        @Override
        public Object getItem(int i) {
            return null;
        }

        @Override
        public long getItemId(int i) {
            return 0;
        }

        @Override
        public View getView(int i, View view, ViewGroup viewGroup) {
            if(view == null){
                view = getLayoutInflater().inflate(R.layout.one_row_design,viewGroup,false);
            }
            TextView t = view.findViewById(R.id.textView);
            ImageView im = view.findViewById(R.id.imageView);
            im.setImageResource(images[i]);
            t.setText(items[i]);
            return view;
        }
    }
}