package com.nareshittechnologies.powerreceiverapp;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.widget.ImageView;
import android.widget.Toast;

public class PowerBroadcastReceiver extends BroadcastReceiver {


    private ImageView imageView;

    public PowerBroadcastReceiver(ImageView imageView) {
        this.imageView = imageView;
    }

    public PowerBroadcastReceiver(){

    }

    @Override
    public void onReceive(Context context, Intent intent) {
        // TODO: This method is called when the BroadcastReceiver is receiving
        // an Intent broadcast.
        if(intent.getAction() == Intent.ACTION_POWER_CONNECTED){
            // do some action for the
            Toast.makeText(context, "POWER CONNECTED", Toast.LENGTH_SHORT).show();
            imageView.setImageResource(R.drawable.charging);
        }else if(intent.getAction() == Intent.ACTION_POWER_DISCONNECTED){
            // do some action for the broadcast
            Toast.makeText(context, "POWER DISCONNECTED", Toast.LENGTH_SHORT).show();
            imageView.setImageResource(R.drawable.discharging);
        }else if(intent.getAction() == MainActivity.ACTION_CUSTOM_BROADCAST){
            Toast.makeText(context, "CUSTOM BROADCAST", Toast.LENGTH_SHORT).show();
        }
    }
}