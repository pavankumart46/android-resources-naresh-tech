package com.nareshittechnologies.powerreceiverapp;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.widget.ImageView;

public class MainActivity extends AppCompatActivity {

    private ImageView imageView;
    public static final String ACTION_CUSTOM_BROADCAST = "com.nareshittechnologies.powerreceiverapp.CUSTOM_BROADCAST";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        imageView = findViewById(R.id.content_image);
        imageView.setImageResource(R.drawable.ic_launcher_background);
        IntentFilter filter = new IntentFilter();
        filter.addAction(Intent.ACTION_POWER_DISCONNECTED);
        filter.addAction(Intent.ACTION_POWER_CONNECTED);

        // Register for braodcasts dynamically
        registerReceiver(new PowerBroadcastReceiver(imageView),filter);

        sendBroadcast(new Intent(this,PowerBroadcastReceiver.class).setAction(ACTION_CUSTOM_BROADCAST));
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        unregisterReceiver(new PowerBroadcastReceiver(imageView));
    }
}


// ACTION_POWER_CONNECTED - This broadcast is delivered by the system when the power source
// is connected to the mobile
// ACTION_POWER_DISCONNECTED