package com.nareshittechnologies.telephonymanager;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;

import android.Manifest;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.telephony.TelephonyManager;
import android.widget.Switch;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    TextView tv;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        tv = findViewById(R.id.textview);

        TelephonyManager manager = (TelephonyManager) getSystemService(TELEPHONY_SERVICE);

        /*if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.M){
            if(checkSelfPermission(Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED
            && checkSelfPermission(Manifest.permission.READ_PHONE_NUMBERS)!= PackageManager.PERMISSION_GRANTED){
                requestPermissions(new String[]{Manifest.permission.READ_PHONE_STATE, Manifest.permission.READ_PHONE_NUMBERS},123);
            }
        }
        tv.setText(String.valueOf(manager.getLine1Number()));*/

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                requestPermissions(new String[]{Manifest.permission.READ_PHONE_STATE},123);
            }
        }

        int phoneType = manager.getPhoneType();
        String phone = "";
        switch (phoneType){
            case TelephonyManager.PHONE_TYPE_CDMA:
                phone = "CDMA";
                break;
            case TelephonyManager.PHONE_TYPE_GSM:
                phone = "GSM";
                break;
            case TelephonyManager.PHONE_TYPE_NONE:
                phone = "NONE";
                break;
        }

        tv.setText(phone);
    }
}