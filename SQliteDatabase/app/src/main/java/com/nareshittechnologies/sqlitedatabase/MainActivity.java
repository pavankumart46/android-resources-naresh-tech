package com.nareshittechnologies.sqlitedatabase;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.ContentValues;
import android.database.Cursor;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    EditText name, age, stu_id;
    DatabaseHelper dbHelper;
    RecyclerView recyclerView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        name = findViewById(R.id.et_name);
        age = findViewById(R.id.et_age);
        stu_id = findViewById(R.id.student_id);
        dbHelper = new DatabaseHelper(this);
        recyclerView = findViewById(R.id.recyclerview);
    }

    public void saveData(View view) {
        String n = name.getText().toString();
        int a = Integer.parseInt(age.getText().toString());

        ContentValues values = new ContentValues();
        values.put(DatabaseHelper.COL_1,n);
        values.put(DatabaseHelper.COL_2,a);

        dbHelper.insertData(values);
        Toast.makeText(this, "VALUES INSERTED SUCCESSFULLY", Toast.LENGTH_SHORT).show();
    }



    public void updateData(View view) {
        String n = name.getText().toString();
        int a = Integer.parseInt(age.getText().toString());
        int id = Integer.parseInt(stu_id.getText().toString());

        ContentValues values = new ContentValues();
        values.put(DatabaseHelper.COL_1,n);
        values.put(DatabaseHelper.COL_2,a);
        if(idPresentOrNot(id)){
            dbHelper.updateData(id,values);    
        }else{
            Toast.makeText(this, "THE REQUEST CANNOT BE PROCESSED!", Toast.LENGTH_SHORT).show();
        }
        
    }

    public void deleteData(View view) {
        int id = Integer.parseInt(stu_id.getText().toString());
        if(idPresentOrNot(id)){
            dbHelper.deleteData(id);    
        }else{
            Toast.makeText(this, "THE REQUEST CANNOT BE PROCESSED!", Toast.LENGTH_SHORT).show();
        }
        
    }
    
    public boolean idPresentOrNot(int id){
        Cursor c = dbHelper.getAlldata();
        c.moveToFirst();
        do{
            int student_id = c.getInt(0);
            if(id == student_id){
                return true;
            }
        }while(c.moveToNext());
        return false;
    }

    public void loadData(View view) {
        List<Student> students = new ArrayList<>();
        StringBuffer sb = new StringBuffer();
        Cursor c = dbHelper.getAlldata();
        c.moveToFirst();
        do{
            int student_id = c.getInt(0);
            String student_name = c.getString(1);
            int age = c.getInt(2);
            Student s = new Student(student_id,student_name,age);
            students.add(s);
        }while(c.moveToNext());
        DBAdapter dbAdapter = new DBAdapter(students);
        recyclerView.setAdapter(dbAdapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
    }

    class DBAdapter extends RecyclerView.Adapter<DBAdapter.DBHolder>{
        List<Student> students;

        public DBAdapter(List<Student> students) {
            this.students = students;
        }

        @NonNull
        @Override
        public DBHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            return new DBHolder(LayoutInflater.from(MainActivity.this)
                    .inflate(R.layout.one_row_design,parent,false));
        }

        @Override
        public void onBindViewHolder(@NonNull DBHolder holder, int position) {
            holder.i.setText(String.valueOf(students.get(position).getStudent_id()));
            holder.n.setText(students.get(position).getStudent_name());
            holder.a.setText(String.valueOf(students.get(position).getStudent_age()));
        }

        @Override
        public int getItemCount() {
            return students.size();
        }

        public class DBHolder extends RecyclerView.ViewHolder {
            TextView i,n,a;
            public DBHolder(@NonNull View itemView) {
                super(itemView);
                i = itemView.findViewById(R.id.s_id);
                n = itemView.findViewById(R.id.s_name);
                a = itemView.findViewById(R.id.s_age);
            }
        }
    }
}