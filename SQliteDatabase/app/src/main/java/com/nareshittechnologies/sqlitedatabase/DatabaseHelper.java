package com.nareshittechnologies.sqlitedatabase;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.widget.Toast;

import androidx.annotation.Nullable;

public class DatabaseHelper extends SQLiteOpenHelper {


    private Context context;
    public static final String DATABASE_NAME = "students.db";
    public static final int DATABASE_VERSION = 1;
    public static final String TABLE_NAME = "students";
    public static final String COL_0 = "stu_id";
    public static final String COL_1 = "stu_name";
    public static final String COL_2 = "stu_age";


    public DatabaseHelper(@Nullable Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
        this.context = context;
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        String create_command = "Create TABLE "+TABLE_NAME+"("+COL_0+" integer " +
                "primary key AUTOINCREMENT,"+COL_1+" text, "+COL_2+" integer);";
        sqLiteDatabase.execSQL(create_command);
        Toast.makeText(context, "CREATE COMMAND EXECUTED", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {
        sqLiteDatabase.execSQL("drop table "+TABLE_NAME);
        onCreate(sqLiteDatabase);
        Toast.makeText(context, "Dropped the table and reacreating it for the new version!", Toast.LENGTH_SHORT).show();
    }

    public void insertData(ContentValues values){
        SQLiteDatabase db = this.getWritableDatabase();
        db.insert(TABLE_NAME,null,values);
        db.close();
    }

    public Cursor getAlldata(){
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor c = db.rawQuery("select * from "+TABLE_NAME,null);
        return c;
    }

    public void updateData(int student_id, ContentValues values){
        SQLiteDatabase db = this.getWritableDatabase();
        db.update(TABLE_NAME,values,COL_0+"=?",new String[]{String.valueOf(student_id)});
        Toast.makeText(context, "Data updated Successfully!", Toast.LENGTH_SHORT).show();
    }

    public void deleteData(int student_id){
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(TABLE_NAME,COL_0+"=?",new String[]{String.valueOf(student_id)});
        Toast.makeText(context, "Data Deleted Successfully!", Toast.LENGTH_SHORT).show();
    }
}
